.. contents:: Table of Contents:


About
-----

Slpkg is a software package manager that installs, updates and removes packages on `Slackware <http://www.slackware.com/>`_-based systems.
It automatically calculates dependencies and figures out what things need to happen to install packages. 
Slpkg makes it easier to manage groups of machines without the need for manual updates.

Slpkg works in accordance with the standards of the `SlackBuilds.org <https://www.slackbuilds.org>`_ organization to build packages. 
It also uses the Slackware Linux instructions for installing, upgrading or removing packages.

.. image:: https://gitlab.com/dslackw/images/raw/master/slpkg/slpkg_package.png
    :target: https://gitlab.com/dslackw/slpkg


Requirements
------------

.. code-block:: bash

    SQLAlchemy>=1.4.36
    PyYAML>=6.0

Install
-------

Install from the official third-party `SBo repository <https://slackbuilds.org/repository/15.0/system/slpkg/>`_ or directly from source:

.. code-block:: bash

    $ tar xvf slpkg-4.1.5.tar.gz
    $ cd slpkg-4.1.5
    $ ./install.sh


Usage
-----

.. code-block:: bash

    $ slpkg --help

      USAGE: slpkg [OPTIONS] [COMMAND] <packages>

      DESCRIPTION:
        Packaging tool that interacts with the SBo repository.

      COMMANDS:
        update                    Update the package lists.
        upgrade                   Upgrade all the packages.
        build <packages>          Build only the packages.
        install <packages>        Build and install the packages.
        remove <packages>         Remove installed packages.
        find <packages>           Find installed packages.
        search <packages>         Search packages on repository.
        clean-logs                Clean dependencies log tracking.
        clean-tmp                 Deletes all the downloaded sources.

      OPTIONS:
        --yes                     Answer Yes to all questions.
        --jobs                    Set it for multicore systems.
        --resolve-off             Turns off dependency resolving.
        --reinstall               Use this option if you want to upgrade.

        -h, --help                Show this message and exit.
        -v, --version             Print version and exit.

      If you need more information try to use slpkg manpage.


Configuration files
-------------------

.. code-block:: bash

    /etc/slpkg/slpkg.yaml
        General configuration of slpkg

    /etc/slpkg/blacklist.yaml
        Blacklist of packages

Donate
------

If you feel satisfied with this project and want to thanks me make a donation.

.. image:: https://gitlab.com/dslackw/images/raw/master/donate/paypaldonate.png
   :target: https://www.paypal.me/dslackw


Copyright
---------

- Copyright 2014-2022 © Dimitris Zlatanidis. 
- Slackware® is a Registered Trademark of Patrick Volkerding. 
- Linux is a Registered Trademark of Linus Torvalds.
